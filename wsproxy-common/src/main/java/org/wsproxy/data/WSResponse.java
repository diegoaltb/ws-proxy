package org.wsproxy.data;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;
import java.util.List;
import java.util.UUID;

@Data
@AllArgsConstructor
public class WSResponse implements Serializable {
    private UUID id;
    private List<WSHeaders> headers;
    private int status;
    private byte[] body;
}
