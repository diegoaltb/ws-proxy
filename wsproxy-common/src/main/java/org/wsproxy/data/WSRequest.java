package org.wsproxy.data;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;
import java.util.List;
import java.util.UUID;

@Data
@AllArgsConstructor
public class WSRequest implements Serializable {
    private UUID id;
    private String path;
    private List<WSHeaders> headers;
    private String method;
    private byte[] body;
}
