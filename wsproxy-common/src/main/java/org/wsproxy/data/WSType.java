package org.wsproxy.data;

public enum WSType {
    REQUEST, RESPONSE, PING
}
