package org.wsproxy.data;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

@Data
@AllArgsConstructor
public class WSHeaders implements Serializable {
    private String key;
    private String value;
}
