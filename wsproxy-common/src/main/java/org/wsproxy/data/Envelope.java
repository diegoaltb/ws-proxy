package org.wsproxy.data;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Base64;

@Data
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class Envelope implements Serializable {
    public static final Envelope PING = new Envelope(WSType.PING, null, null);

    public static final String ENCODED_PING;

    static {
        String ENCODED_PING1;

        try (ByteArrayOutputStream baos = new ByteArrayOutputStream(); ObjectOutputStream oos = new ObjectOutputStream(baos)) {
            oos.writeObject(PING);
            oos.flush();
            baos.flush();
            ENCODED_PING1 = Base64.getUrlEncoder().encodeToString(baos.toByteArray());
        } catch (IOException e) {
            ENCODED_PING1 = "PING";
        }
        ENCODED_PING = ENCODED_PING1;
    }


    private WSType mode;
    private WSRequest request;
    private WSResponse response;

    public static Envelope request(WSRequest request) {
        return new Envelope(WSType.REQUEST, request, null);
    }

    public static Envelope response(WSResponse response) {
        return new Envelope(WSType.RESPONSE, null, response);
    }
}
