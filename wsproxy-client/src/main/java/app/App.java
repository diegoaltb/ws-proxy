package app;

import okhttp3.*;
import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;
import org.wsproxy.data.*;

import java.io.*;
import java.net.URI;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

public class App {
    public static final Logger log = Logger.getAnonymousLogger();
    private static final ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();

    private static void usage() {
        System.err.println("wsproxy <url> <port> <name> <MR>");
    }

    public static void main(final String[] args) {

        if (args.length != 4) {
            usage();
            System.exit(101);
        }

        int port = 0;
        try {
            port = Integer.parseInt(args[1]);
        } catch (NumberFormatException nfe) {
            usage();
            System.exit(101);

        }
        int MR = 0;
        try {
            MR = Integer.parseInt(args[3]);
        } catch (NumberFormatException nfe) {
            usage();
            System.exit(101);
        }

        String url = args[0];
        if (url.length() < 6) {
            usage();
            return;
        }
        String prefix = url.substring(0, 6);
        if (!prefix.contains("ws") || !prefix.contains("://")) {
            url = "wss://" + url;
        }

        String finalUrl = url;
        int finalPort = port;

        OkHttpClient client = new OkHttpClient();
        URI server = URI.create(finalUrl + "/register?branch=" + args[2] + "&MR=" + MR);


        WebSocketClient webSocketClient = new WebSocketClient(server) {
            {
                setConnectionLostTimeout(0);
            }
            @Override
            public void onOpen(ServerHandshake serverHandshake) {
                log.info("Connected to " + finalUrl);
                send(Envelope.ENCODED_PING);
            }

            @Override
            public void onMessage(String text) {
                try (ByteArrayInputStream bais = new ByteArrayInputStream(Base64.getUrlDecoder().decode(text));
                     ObjectInputStream ois = new ObjectInputStream(bais);
                     ByteArrayOutputStream baos = new ByteArrayOutputStream();
                     ObjectOutputStream oos = new ObjectOutputStream(baos)) {

                    Envelope envelope = (Envelope) ois.readObject();
                    if (envelope.getMode() == WSType.PING) {
                        executorService.schedule(() -> send(Envelope.ENCODED_PING), 5, TimeUnit.SECONDS);
                    } else if (envelope.getMode() == WSType.REQUEST) {
                        WSRequest request = envelope.getRequest();
                        String path = request.getPath();
                        Envelope defaultResponse = Envelope.response(new WSResponse(request.getId(), new ArrayList<>(), 500, null));

                        log.info("Received request on " + path);

                        try {
                            Request.Builder builder = new Request.Builder();
                            if (request.getHeaders() != null) {
                                request.getHeaders().forEach(wsHeaders -> builder.addHeader(wsHeaders.getKey(), wsHeaders.getValue()));
                            }
                            builder.url("http://localhost:" + finalPort + "/" + path);
                            builder.method(request.getMethod(), Optional.ofNullable(request.getBody())
                                    .filter(e -> e.length > 0)
                                    .map(RequestBody::create)
                                    .orElse(null));
                            Request req = builder.build();
                            Response rsp = client.newCall(req).execute();
                            int status = rsp.code();
                            byte[] body = null;
                            ResponseBody responseBody = rsp.body();
                            if (responseBody != null)
                                body = responseBody.bytes();
                            List<WSHeaders> headers = new ArrayList<>();
                            rsp.headers().forEach(p -> headers.add(new WSHeaders(p.getFirst(), p.getSecond())));
                            WSResponse response = new WSResponse(request.getId(), headers, status, body);
                            oos.writeObject(Envelope.response(response));
                            oos.flush();
                            baos.flush();
                            send(Base64.getUrlEncoder().encodeToString(baos.toByteArray()));
                        } catch (IllegalArgumentException | IOException e) {
                            log.info("Server not ready or wrong body usage");
                            oos.writeObject(defaultResponse);
                            oos.flush();
                            baos.flush();
                            send(Base64.getUrlEncoder().encodeToString(baos.toByteArray()));
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    close(1013, "Bad connection");
                    System.exit(1013);
                } catch (ClassCastException | ClassNotFoundException e) {
                    close(1011, "Wrong usage");
                    System.exit(1011);
                }
            }

            @Override
            public void onClose(int code, String reason, boolean b) {
                log.info(reason);
                System.exit(code == 1000 ? 0 : 1);
            }

            @Override
            public void onError(Exception e) {
                e.printStackTrace();
                System.exit(101);
            }
        };
        webSocketClient.connect();
    }

}
