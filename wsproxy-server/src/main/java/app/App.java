package app;

import com.google.common.collect.ImmutableMap;
import com.google.common.io.ByteStreams;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import io.jooby.*;
import io.jooby.exception.StatusCodeException;
import io.jooby.exception.UnsupportedMediaType;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.wsproxy.data.*;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.*;
import java.util.logging.Logger;
import java.util.stream.Collectors;


public class App extends Jooby {
    private static final ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();

    private static final Logger log = Logger.getAnonymousLogger();
    private static final MessageDecoder generalDecoder = (ctx, type) -> {
        if (type != byte[].class)
            throw new UnsupportedMediaType(ctx.header("Content-Type").valueOrNull());

        Body body = ctx.body();

        if (body.isInMemory()) return body.bytes();
        else return ByteStreams.toByteArray(body.stream());
    };
    private final ConcurrentMap<UUID, NamedSocket> connections = new ConcurrentHashMap<>();
    private final ConcurrentMap<UUID, Contextual> callbacks = new ConcurrentHashMap<>();

    public App() {
        super();
        addFormDecoder();
        addOctetDecoder();
        Gson gson = new GsonBuilder().create();
        encoder((Context ctx, Object value) -> {
            ctx.setDefaultResponseType(MediaType.json);
            return gson.toJson(value).getBytes(StandardCharsets.UTF_8);
        });

        addWebsocket();

        get("/available", ctx -> ImmutableMap.of("servers", connections.entrySet().stream()
                .map(e -> ImmutableMap.of(
                        "name", e.getValue().name,
                        "MR", e.getValue().MR,
                        "path", Base64.getUrlEncoder().encodeToString(e.getKey().toString().getBytes(StandardCharsets.UTF_8)),
                        "createdAt", e.getValue().time.format(DateTimeFormatter.ofPattern("dd/M HH:mm"))))
                .collect(Collectors.toList())));


        get("/approve/{uuid}", ctx -> {
            String uuids = ctx.path("uuid").value();
            UUID uuid = UUID.fromString(new String(Base64.getUrlDecoder().decode(uuids), StandardCharsets.UTF_8));
            NamedSocket namedSocket = connections.remove(uuid);
            if (namedSocket != null) {
                namedSocket.socket.close(WebSocketCloseStatus.NORMAL);
                return ctx.send(StatusCode.OK);
            } else throw new StatusCodeException(StatusCode.NOT_FOUND);
        });

        get("/reprove/{uuid}", ctx -> {
            String uuids = ctx.path("uuid").value();
            UUID uuid = UUID.fromString(new String(Base64.getUrlDecoder().decode(uuids), StandardCharsets.UTF_8));
            NamedSocket namedSocket = connections.remove(uuid);
            if (namedSocket != null) {
                namedSocket.socket.close(WebSocketCloseStatus.BAD_DATA);
                return ctx.send(StatusCode.OK);
            } else throw new StatusCodeException(StatusCode.NOT_FOUND);
        });


        path("/forward/{uuid}/*", () -> {
            decorator(next -> ctx -> {
                String path = ctx.path("*").value() + ctx.query().queryString();
                log.info("New request (" + ctx.getMethod() + ") on " + path);
                String uuids = ctx.path("uuid").value();
                UUID uuid = UUID.fromString(new String(Base64.getUrlDecoder().decode(uuids), StandardCharsets.UTF_8));
                NamedSocket ns = connections.get(uuid);
                if (ns != null) {
                    List<WSHeaders> headersList = new ArrayList<>();
                    ctx.headerMultimap().forEach((k, l) -> l.forEach(v ->
                            headersList.add(new WSHeaders(k, v))
                    ));
                    byte[] bodyArray = ctx.decode(byte[].class, ctx.getRequestType(MediaType.text));
                    CompletableFuture<Context> cf = new CompletableFuture<>();
                    UUID id = UUID.randomUUID();

                    callbacks.put(id, new Contextual(ctx, cf));
                    try (ByteArrayOutputStream baos = new ByteArrayOutputStream(); ObjectOutputStream oos = new ObjectOutputStream(baos)) {
                        WSRequest wsRequest = new WSRequest(id, path, headersList, ctx.getMethod(), bodyArray);
                        oos.writeObject(Envelope.request(wsRequest));
                        oos.flush();
                        baos.flush();
                        String message = Base64.getUrlEncoder().encodeToString(baos.toByteArray());
                        ns.socket.send(message);
                        log.info("Message sent with path " + path);
                    }
                    return cf;

                } else throw new StatusCodeException(StatusCode.NOT_FOUND);
            });
            Router.METHODS.forEach(m -> route(m, "*", ctx -> ctx));
        });
    }

    public static void main(final String[] args) {
        runApp(args, App::new);
    }

    private void addWebsocket() {
        ws("/register", ((ctx, configurer) -> {
            configureOnConnect(ctx, configurer);

            configureOnError(configurer);

            configureOnClose(configurer);

            configureOnMessage(configurer);
        }));
    }

    private void configureOnMessage(WebSocketConfigurer configurer) {
        configurer.onMessage(((ws, message) -> {
            String text = message.value();
            try (ByteArrayInputStream bais = new ByteArrayInputStream(Base64.getUrlDecoder().decode(text)); ObjectInputStream ois = new ObjectInputStream(bais)) {
                Envelope envelope = (Envelope) ois.readObject();
                if (envelope.getMode() == WSType.PING) {
                    executorService.schedule(() -> ws.send(Envelope.ENCODED_PING), 5, TimeUnit.SECONDS);
                } else if (envelope.getMode() == WSType.RESPONSE) {
                    WSResponse response = envelope.getResponse();
                    Contextual contextual = callbacks.remove(response.getId());
                    if (contextual != null) {
                        Context respCtx = contextual.context;
                        if (response.getHeaders() != null)
                            response.getHeaders().forEach(wsHeaders -> respCtx.setResponseHeader(wsHeaders.getKey(), wsHeaders.getValue()));

                        contextual.future.complete(respCtx.setResponseCode(response.getStatus()).send(Optional.ofNullable(response.getBody()).orElse(new byte[0])));
                    }
                }
            } catch (IOException e) {
                ws.close(WebSocketCloseStatus.PROTOCOL_ERROR);
            } catch (ClassNotFoundException | ClassCastException e) {
                ws.close(WebSocketCloseStatus.BAD_DATA);
            }
        }));
    }

    private void configureOnClose(WebSocketConfigurer configurer) {
        configurer.onClose(((ws, closeStatus) -> {
            NamedSocket ns = connections.remove(ws.<UUID>attribute("id"));
            if (ns != null) log.info("Connection removed: " + ns.name);
        }));
    }

    private void configureOnError(WebSocketConfigurer configurer) {
        configurer.onError(((ws, cause) -> {
            NamedSocket ns = connections.remove(ws.<UUID>attribute("id"));
            if (ns != null) log.info("Connection removed: " + ns.name + " (" + cause + ")");
        }));
    }

    private void configureOnConnect(Context ctx, WebSocketConfigurer configurer) {
        configurer.onConnect(ws -> {
            String branch = ctx.query("branch").value();
            int MR = ctx.query("MR").intValue();
            UUID id = UUID.randomUUID();
            connections.put(id, new NamedSocket(branch, MR, ws.attribute("id", id), ZonedDateTime.now(ZoneId.of("GMT-3"))));
            log.info("Connection added: " + branch);
        });
    }

    private void addOctetDecoder() {
        decoder(MediaType.octetStream, generalDecoder);
        decoder(MediaType.json, generalDecoder);
        decoder(MediaType.valueOf("image/jpeg"), generalDecoder);
    }

    private void addFormDecoder() {
        decoder(MediaType.form, (ctx, type) -> {
            if (type != byte[].class)
                throw new UnsupportedMediaType(ctx.header("Content-Type").valueOrNull());

            Formdata form = ctx.form();
            return form.value(e -> e.getBytes(StandardCharsets.UTF_8));
        });
    }

    @Data
    @AllArgsConstructor
    public static class NamedSocket {
        private String name;
        private int MR;
        private WebSocket socket;
        private ZonedDateTime time;
    }

    @Data
    @AllArgsConstructor
    public static class Contextual {
        private Context context;
        private CompletableFuture<Context> future;
    }

}
